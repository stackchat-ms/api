COMPOSE_FILES = -f packages/chat/docker-compose.yml -f packages/identity/docker-compose.yml -f packages/rooms/docker-compose.yml

dev:
	docker-compose $(COMPOSE_FILES) up

rebuild:
	docker-compose $(COMPOSE_FILES) build --no-cache

lint:
	npx lerna run lint

test:
	npx lerna run test

install:
	npx lerna bootstrap