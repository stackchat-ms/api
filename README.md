# StackChat API

This is an overengineered chat application.

## Local development

### Working with all microservices

```
make dev // Start all

make lint // Lint all
make test // Test all
make rebuild // Rebuild all dockerfiles
make install // Install all NPM dependencies
```

### Working with one microservice at a time


## Environments

### Local

**Identity API**: runs on http://localhost:8000/

**Rooms API**: runs on http://localhost:8001/

**Chat API**: runs on http://localhost:8002/

### Staging

**Identity API**: runs on https://stackchat-identity-dev.herokuapp.com/

**Rooms API**: runs on https://stackchat-rooms-dev.herokuapp.com/

**Chat API**: runs on https://stackchat-chat-dev.herokuapp.com/