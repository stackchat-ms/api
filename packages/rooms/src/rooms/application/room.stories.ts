import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Room } from '../infrastructure/database/entities/room.entity';

interface CreateRoom {
  name: string;
  password?: string;
  knock?: boolean;
  media?: boolean;
}

@Injectable()
export class RoomStories {
  constructor(
    @InjectRepository(Room)
    private readonly roomRepository: Repository<Room>,
  ) {}

  async create(room: CreateRoom) {
    const createdRoom = this.roomRepository.create({
      name: room.name,
      password: room.password,
      knock: room.knock,
      media: room.media,
    });

    return await this.roomRepository.save(createdRoom);
  }

  async getRooms() {
    const rooms = await this.roomRepository.find();

    return rooms;
  }
}
