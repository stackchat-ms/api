import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtStrategy } from '@stackchat/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Constants } from '../constants';
import { Room } from './infrastructure/database/entities/room.entity';
import { RoomStories } from './application/room.stories';
import { RoomResolver } from './infrastructure/graphql/resolvers/room.resolver';

@Module({
  imports: [
    TypeOrmModule.forFeature([Room]),
    PassportModule,
    JwtModule.register({
      secret: Constants.secret,
      signOptions: { expiresIn: '24h' },
    }),
  ],
  providers: [JwtStrategy, RoomStories, RoomResolver],
  exports: [TypeOrmModule],
})
export class RoomModule {}
