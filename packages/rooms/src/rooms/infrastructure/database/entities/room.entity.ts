import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Room {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  password?: string;

  @Column({ default: true })
  knock: boolean;

  @Column({ default: true })
  media: boolean;
}
