import { Field, ObjectType } from '@nestjs/graphql';

import { Room as RoomEntity } from '../../database/entities/room.entity';

@ObjectType()
export class Room {
  @Field()
  id: number;

  @Field()
  name: string;

  @Field()
  knock: boolean;

  @Field()
  media: boolean;
}

export const roomEntityToModel = (entity: RoomEntity): Room => {
  return {
    id: entity.id,
    name: entity.name,
    knock: entity.knock,
    media: entity.media,
  };
};
