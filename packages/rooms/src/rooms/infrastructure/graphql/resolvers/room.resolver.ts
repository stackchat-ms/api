import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { GqlAuthGuard } from '@stackchat/common';
import { UseGuards } from '@nestjs/common';
import { RoomStories } from '../../../application/room.stories';
import { Room, roomEntityToModel } from '../models/room.model';

@Resolver((of) => Room)
export class RoomResolver {
  constructor(private readonly roomStories: RoomStories) {}

  @UseGuards(GqlAuthGuard)
  @Query((returns) => [Room])
  async rooms(): Promise<Room[]> {
    const rooms = await this.roomStories.getRooms();
    return rooms.map(roomEntityToModel);
  }

  @Mutation((returns) => Room)
  async create(
    @Args('name') name: string,
    @Args('knock', { nullable: true }) knock?: boolean,
    @Args('media', { nullable: true }) media?: boolean,
    @Args('password', { nullable: true }) password?: string,
  ): Promise<Room> {
    const room = await this.roomStories.create({
      name,
      password,
      knock,
      media,
    });

    return roomEntityToModel(room);
  }
}
