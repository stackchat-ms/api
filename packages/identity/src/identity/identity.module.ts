import { JwtStrategy } from '@stackchat/common';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { User } from './infrastructure/database/entities/user.entity';
import { UserResolver } from './infrastructure/graphql/resolvers/user.resolver';
import { UserStories } from './application/user.stories';
import { Constants } from '../constants';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    PassportModule,
    JwtModule.register({
      secret: Constants.secret,
      signOptions: { expiresIn: '24h' },
    }),
  ],
  providers: [UserResolver, UserStories, JwtStrategy],
  exports: [TypeOrmModule],
})
export class IdentityModule {}
