import { Field, ObjectType } from '@nestjs/graphql';
import { User as UserEntity } from '../../database/entities/user.entity';

export type UserWithoutPassword = Omit<User, 'password'>;

@ObjectType()
export class User {
  @Field()
  id: number;

  @Field()
  username: string;
}

export const userEntityToModel = (entity: UserEntity): User => {
  return {
    id: entity.id,
    username: entity.username,
  };
};
