import { ContextUser, CurrentUser, GqlAuthGuard } from '@stackchat/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { User, userEntityToModel } from '../models/user.model';
import { UserStories } from '../../../application/user.stories';
import { AccessToken } from '../models/jwt.model';

@Resolver((of) => User)
export class UserResolver {
  constructor(private readonly userStories: UserStories) {}

  @Query((returns) => User)
  @UseGuards(GqlAuthGuard)
  async me(@CurrentUser() currentUser: ContextUser): Promise<User> {
    return userEntityToModel(
      await this.userStories.getUser(currentUser.userId),
    );
  }

  @Query((returns) => [User])
  async users(): Promise<User[]> {
    const users = await this.userStories.getUsers();
    return users.map(userEntityToModel);
  }

  @Query((returns) => AccessToken)
  async login(
    @Args('username') username: string,
    @Args('password') password: string,
  ): Promise<AccessToken> {
    const user = await this.userStories.validateCredentials(username, password);
    const accessToken = await this.userStories.login(user);

    return accessToken;
  }

  @Mutation((returns) => User)
  async register(
    @Args('username') username: string,
    @Args('password') password: string,
  ): Promise<User> {
    const user = await this.userStories.register(username, password);

    return userEntityToModel(user);
  }
}
