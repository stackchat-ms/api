import { Module } from '@nestjs/common';
import { Constants, JwtStrategy } from '@stackchat/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ChatResolver } from './infrastructure/graphql/resolvers/chat.resolver';
import { Message } from './infrastructure/graphql/models/Message.model';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: Constants.secret,
      signOptions: { expiresIn: '24h' },
    }),
  ],
  providers: [JwtStrategy, ChatResolver, Message],
  exports: [],
})
export class ChatModule {}
