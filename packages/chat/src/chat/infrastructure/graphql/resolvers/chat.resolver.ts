import { Args, Query, Mutation, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';
import { Message } from '../models/Message.model';

const pubSub = new PubSub();

const messages: Message[] = [];

@Resolver((of) => Message)
export class ChatResolver {
  @Query((returns) => [Message])
  messages() {
    return messages;
  }

  @Mutation((returns) => Message)
  async postMessage(@Args('content') content: string) {
    const message: Message = {
      id: 1,
      content,
    };

    messages.push(message);

    pubSub.publish('messagePosted', { messagePosted: message });

    return message;
  }

  @Subscription((returns) => Message)
  messagePosted() {
    return pubSub.asyncIterator('messagePosted');
  }
}
